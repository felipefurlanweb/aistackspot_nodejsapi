const express = require('express');
const jwt = require('jsonwebtoken');
const mysql = require('mysql2');
const app = express();
const port = 3000;

const chaveSecreta = 'sua_chave_secreta';

// Configuração da conexão com o banco de dados
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'aistackspotnode'
  });

app.use(express.json());

// Middleware para validar o Bearer Token
const validarToken = (req, res, next) => {
    const token = req.headers.authorization;  
    if (token) {
        try {
            const tokenValue = token.split(' ')[1];
          
            // Aqui você pode adicionar a lógica de validação do token, por exemplo, verificar se é um token válido
            const decoded = jwt.verify(tokenValue, chaveSecreta);
    
            req.usuario = decoded; // Armazena os dados do usuário decodificados no objeto de requisição
            next(); // Chama o próximo middleware ou a rota
        } catch (error) {
            res.status(401).json({ error: 'Token inválido' });
        }
    } else {
    res.status(401).json({ error: 'Token não fornecido' });
    }
};

// Rota de login
app.post('/api/login', (req, res) => {
    // Aqui você pode adicionar a lógica de autenticação, por exemplo, verificar se as credenciais são válidas
    if(req.body.username != "felipefurlanweb"){
        // Retorna status 401 (Unauthorized)
        res.status(401).json({ error: 'Usuário inválido' });
    }

    // Se as credenciais forem válidas, gere o token JWT
    const token = jwt.sign({ id: req.body.username, username: req.body.username }, chaveSecreta);
  
    // Retorne o token na resposta
    res.json({ token });
});

// Rota GET para a raiz da API
app.get('/', (req, res) => {
  res.send('Olá, mundo!');
});

app.get('/api/usuario/:id', (req, res) => {
    const id = req.params.id;
  
    // Query de busca
    const query = 'SELECT * FROM usuarios WHERE id = ?';
  
    // Executa a query de busca
    connection.query(query, [id], (error, results) => {
      if (error) {
        console.error('Erro ao buscar usuário:', error);
        res.status(500).json({ error: 'Erro ao buscar usuário' });
      } else {
        if (results.length > 0) {
          const usuario = results[0];
          res.json(usuario);
        } else {
          res.status(404).json({ error: 'Usuário não encontrado' });
        }
      }
    });
  });

// Rota POST
app.post('/api/usuario', validarToken, (req, res) => {
    const usuario = req.body;

    // Query de inserção
    const query = 'INSERT INTO usuarios (nome, email, data_nascimento) VALUES (?, ?, ?)';

    // Valores a serem inseridos
    const values = [usuario.nome, usuario.email, usuario.data_nascimento];

    // Executa a query de inserção
    connection.query(query, values, (error, results) => {
        if (error) {
            console.error('Erro ao inserir usuário:', error);
            res.status(500).json({ error: 'Erro ao inserir usuário' });
        } else {
            console.log('Usuário inserido com sucesso');
            res.json(usuario);
        }
    });
});

app.delete('/api/usuario/:id', (req, res) => {
    const id = req.params.id;
  
    // Verifica se o usuário existe antes de remover
    const checkQuery = 'SELECT * FROM usuarios WHERE id = ?';
    connection.query(checkQuery, [id], (error, results) => {
      if (error) {
        console.error('Erro ao verificar usuário:', error);
        res.status(500).json({ error: 'Erro ao verificar usuário' });
      } else {
        if (results.length > 0) {
          // Remove o usuário caso exista
          const deleteQuery = 'DELETE FROM usuarios WHERE id = ?';
          connection.query(deleteQuery, [id], (error) => {
            if (error) {
              console.error('Erro ao remover usuário:', error);
              res.status(500).json({ error: 'Erro ao remover usuário' });
            } else {
              console.log('Usuário removido com sucesso');
              res.json({ message: 'Usuário removido com sucesso' });
            }
          });
        } else {
          res.status(404).json({ error: 'Usuário não encontrado' });
        }
      }
    });
});

app.put('/api/usuario/:id', (req, res) => {
    const id = req.params.id;
    const novoUsuario = req.body;
  
    // Verifica se o usuário existe antes de atualizar
    const checkQuery = 'SELECT * FROM usuarios WHERE id = ?';
    connection.query(checkQuery, [id], (error, results) => {
      if (error) {
        console.error('Erro ao verificar usuário:', error);
        res.status(500).json({ error: 'Erro ao verificar usuário' });
      } else {
        if (results.length > 0) {
          // Atualiza o usuário caso exista
          const updateQuery = 'UPDATE usuarios SET nome = ?, email = ?, data_nascimento = ? WHERE id = ?';
          const values = [novoUsuario.nome, novoUsuario.email, novoUsuario.data_nascimento, id];
          connection.query(updateQuery, values, (error) => {
            if (error) {
              console.error('Erro ao atualizar usuário:', error);
              res.status(500).json({ error: 'Erro ao atualizar usuário' });
            } else {
              console.log('Usuário atualizado com sucesso');
              res.json(novoUsuario);
            }
          });
        } else {
          res.status(404).json({ error: 'Usuário não encontrado' });
        }
      }
    });
});

// Iniciar o servidor
app.listen(port, () => {
  console.log(`Servidor rodando na porta ${port}`);
});